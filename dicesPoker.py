#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Jeu de dés

from random import randint

tempScorePlayer1 = 0
scorePlayer1 = 0
tempScorePlayer2 = 0
scorePlayer2 = 0

dicesPlayer1 = []
dicesPlayer2 = []
dicesScores = []
emptyList = []

def dice(val1, val2):
    result = randint(val1, val2)
    return(result)

def check(tempScore):
    pairs = []
    triple = []
    four = []
    quinte = []
    straight1 = [1, 2, 3, 4, 5]
    straight2 = [2, 3, 4, 5, 6]
    score = 0

    for x in tempScore:
        if tempScore.count(x) > 1 and tempScore.count(x) < 3:
            pairs.append(x)
    for x in tempScore:
        if tempScore.count(x) == 3:
            triple.append(x)
    for x in tempScore:
        if tempScore.count(x) == 4:
            four.append(x)
    for x in tempScore:
        if tempScore.count(x) == 5:
            quinte.append(x)
    
    # Les lignes suivantes étaient utilisées pour debug        
    """ print("")
    print("pairs : {} | triple : {} | four : {}".format(pairs, triple, four))
    print("") """

    # permet la comparaison avec les deux suites
    tempScore.sort()

    # Cette condition est source de bug si on ne test pas ce qu'il y a dans triple !
    if len(pairs) == 2 and len(triple) != 3 :
        print(" Tu as une paire de {}".format(pairs[0]))
        score = 1
    
    elif len(pairs) == 4:
        pairs.sort()
        print(" Tu as une paires de {} et une paire de {}".format(pairs[0], pairs[2]))
        score = 2
    
    elif len(triple) == 3 and len(pairs) != 2:
        print("Tu as un Brelan de {}".format(triple[0]))
        score = 3

    elif len(triple) == 3 and len(pairs) == 2:
        print(" Tu as un Full ! Trois {} et une paire de {}".format(triple[0], pairs[0]))
        score = 5
    
    elif len(four) == 4:
        print(" Tu as un Carré de {}".format(four[0]))
        score = 8
    
    # cette condition ne fonctionne pas si on ne trie pas tempScore !
    elif tempScore == straight1 or tempScore == straight2:
        print(" Tu as une suite ! {}".format(tempScore))
        score = 10
    
    elif len(quinte) == 5:
        print(" Tu as une Quinte ! Cinq {} !".format(quinte[0]))
        score = 15

    else:
        print(" Ta main est nulle ! Bhouuu !")

    return score

def reroll(tempDices, p):
    for i in range(0,5):
        print(" Le tirage actuel est {}".format(tempDices))
        r = raw_input(" Joueur {} : Conserver ce {} ou Relancer le dé ? (c/r) :".format(p, tempDices[i]))
        if r == "r" or r == "R":
            tempDices[i]=dice(1,6)
    # Comment supprimer la ligne suivante tout en affichant bien le résultat trié dans le print ?
    tempDices.sort()
    print(" Le tirage final du joueur {} est {}".format(p, tempDices))
    return tempDices

while(True):

    # Tour du joueur 1
    player = 1
    print(" C'est le tour du joueur {}".format(player))

    for score in range(1,6) :
        dicesPlayer1.append(dice(1,6))

    dicesPlayer1.sort()
    print(" Premier tirage pour le joueur {} : {}".format(player, dicesPlayer1))
    tempScorePlayer1 = check(dicesPlayer1)
    r = raw_input(" Joueur {}: Conserver ce tirage ou Relancer des dès ? (c/r) :".format(player))
    if r == "r" or r == "R":
        dicesPlayer1 = list(reroll(dicesPlayer1, player))
        scorePlayer1 = scorePlayer1 + check(dicesPlayer1)
    else :
        scorePlayer1 = scorePlayer1 + tempScorePlayer1
        tempScorePlayer1 = 0
    dicesPlayer1 = list(emptyList)
    print(" Score du joueur {} : {} points".format(player, scorePlayer1))
    print("")

    # Tour du joueur 2
    player = 2
    print(" C'est le tour du joueur {}".format(player))

    for score in range(1,6) :
        dicesPlayer2.append(dice(1,6))

    dicesPlayer2.sort()
    print(" Premier tirage pour le joueur {} : {}".format(player, dicesPlayer2))
    tempScorePlayer2 = check(dicesPlayer2)
    r = raw_input(" Joueur {}: Conserver ce tirage ou Relancer des dès ? (c/r) :".format(player))
    if r == "r" or r == "R":
        dicesPlayer2 = list(reroll(dicesPlayer2, player))
        scorePlayer2 = scorePlayer2 + check(dicesPlayer2)
    else :
        scorePlayer2 = scorePlayer2 + tempScorePlayer2
        tempScorePlayer2 = 0
    dicesPlayer2 = list(emptyList)
    print(" Score du joueur {} : {} points".format(player, scorePlayer2))
    print("")

    # Fin de partie ou continuer
    print(" Les scores sont de : ")
    print(" -> Joueur 1 : {} points".format(scorePlayer1))
    print(" -> Joueur 2 : {} points".format(scorePlayer2))
    
    r = raw_input(" Voulez vous continuer la partie ? (o/n) :")
    if r == "n" or r == "N":
        if scorePlayer1 > scorePlayer2:
            print(" *** Le jouer 1 gagne la partie ! ***")
        elif scorePlayer2 > scorePlayer1:
            print(" *** Le jouer 2 gagne la partie ! ***")
        elif scorePlayer1 == scorePlayer2:
            print(" *** Égalité ! ***")
        
        print(" À bientôt !")
        break