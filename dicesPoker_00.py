#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Jeu de dés

# Depuis la librairie random on importe la fonction randint
# Randint permet de tirer un nombre aléatoire
from random import randint

# On initialise le tableau des scores des deux joueurs
# Les deux joueurs commencent à 0
scores = [0,0]

# Fonction de lancé d'un seul dé
# Le résultat sera un chiffre entre 1 et 6, inclus
def dice(val1, val2):
    result = randint(val1, val2)
    return(result)

# Fonction d'évaluation du résultat d'un tirage
def check(dices):
    pairs = []                      # Liste des paires
    triple = []                     # Liste des triples
    four = []                       # Liste des quadruples
    quinte = []                     # Liste des quintuples
    straight1 = [1, 2, 3, 4, 5]     # La suite faible
    straight2 = [2, 3, 4, 5, 6]     # La suite forte
    score = 0                       # Le score de ce lancé est mis à 0

    # Pour chaque dés du tirage on va chercher à savoir combien
    # d'autre dés ont le même chiffre et donc ajouter les chiffres
    # en double à une liste des paires, en triples à la liste des triples, etc.
    # dices.count(d) veut dire : dans la liste "dices" combien de fois trouve t'on le chiffre "d"

    # Test du nombre de paires ou doubles
    for d in dices:
        if dices.count(d) > 1 and dices.count(d) < 3:
            pairs.append(d)

    # Test du nombre de triples
        if dices.count(d) == 3:
            triple.append(d)
    
    # Test du nombre de quadrules
        if dices.count(d) == 4:
            four.append(d)
    
    # Test du nombre de quintuples
        if dices.count(d) == 5:
            quinte.append(d)

    # On range la liste des score dansPermet la comparaison avec les deux suites
    dices.sort()

    # Cette condition est source de bug si on ne test pas ce qu'il y a dans triple !
    if len(pairs) == 2 and len(triple) != 3 :
        print(" Tu as une paire de {}".format(pairs[0]))
        score = 1
    
    elif len(pairs) == 4:
        pairs.sort()
        print(" Tu as une paires de {} et une paire de {}".format(pairs[0], pairs[2]))
        score = 2
    
    elif len(triple) == 3 and len(pairs) != 2:
        print("Tu as un Brelan de {}".format(triple[0]))
        score = 3

    elif len(triple) == 3 and len(pairs) == 2:
        print(" Tu as un Full ! Trois {} et une paire de {}".format(triple[0], pairs[0]))
        score = 5
    
    elif len(four) == 4:
        print(" Tu as un Carré de {}".format(four[0]))
        score = 8
    
    # cette condition ne fonctionne pas si on ne trie pas dices !
    elif dices == straight1 or dices == straight2:
        print(" Tu as une suite ! {}".format(dices))
        score = 10
    
    elif len(quinte) == 5:
        print(" Tu as une Quinte ! Cinq {} !".format(quinte[0]))
        score = 15

    else:
        print(" Désolé, tu n'a pas de combinaison !")

    return score

def reroll(p, tempDices):
    for i in range(0,5):
        
        if i == 0 :
            print(" [{}], {}, {}, {}, {}".format(tempDices[0], tempDices[1], tempDices[2], tempDices[3], tempDices[4]))
        elif i == 1 :
            print(" {}, [{}], {}, {}, {}".format(tempDices[0], tempDices[1], tempDices[2], tempDices[3], tempDices[4]))
        elif i == 2 :
            print(" {}, {}, [{}], {}, {}".format(tempDices[0], tempDices[1], tempDices[2], tempDices[3], tempDices[4]))
        elif i == 3 :
            print(" {}, {}, {}, [{}], {}".format(tempDices[0], tempDices[1], tempDices[2], tempDices[3], tempDices[4]))
        elif i == 4 :
            print(" {}, {}, {}, {}, [{}]".format(tempDices[0], tempDices[1], tempDices[2], tempDices[3], tempDices[4]))
        
        r = raw_input(" Joueur {} : Conserver ce {} ou Relancer le dé ? (c/r) :".format(p, tempDices[i]))
        if r == "r" or r == "R":
            tempDices[i]=dice(1,6)
            
    # Comment supprimer la ligne suivante tout en affichant bien le résultat trié dans le print ?
    tempDices.sort()
    print(" Le tirage final du joueur {} est : {}, {}, {}, {}, {}".format(p, tempDices[0], tempDices[1], tempDices[2], tempDices[3], tempDices[4]))
    return tempDices

def roll(p):

    tempScore = 0
    score = 0
    dicesPlayer = []

    for d in range(1,6) :
        dicesPlayer.append(dice(1,6))
    
    dicesPlayer.sort()
    print(" Premier tirage pour le joueur {} : {}, {}, {}, {}, {}".format(p, dicesPlayer[0], dicesPlayer[1], dicesPlayer[2], dicesPlayer[3], dicesPlayer[4]))
    tempScore = check(dicesPlayer)
    r = raw_input(" Joueur {}: Conserver ce tirage ou Relancer des dès ? (c/r) :".format(p))
    if r == "r" or r == "R":
        dicesPlayer = list(reroll(p, dicesPlayer))
        score = check(dicesPlayer)
        return score
    else :
        return tempScore

while(True):

    for tour in range(2):
        if tour == 0:
            player = 1
        else:
            player = 2
        print(" C'est le tour du joueur {}".format(player))
        scores[tour] = scores[tour] + roll(player)
        print(" Score du joueur {} : {} points".format(player, scores[tour]))
        print("")

    # Fin de partie ou continuer
    print(" Les scores sont de : ")
    print(" -> Joueur 1 : {} points".format(scores[0]))
    print(" -> Joueur 2 : {} points".format(scores[1]))
    
    r = raw_input(" Voulez vous continuer la partie ? (o/n) :")
    if r != "o":
        if scores[0] > scores[1]:
            print(" *** Le jouer 1 gagne la partie ! ***")
        elif scores[1] > scores[0]:
            print(" *** Le jouer 2 gagne la partie ! ***")
        elif scores[0] == scores[1]:
            print(" *** Égalité ! ***")
        
        print(" À bientôt !")
        break
