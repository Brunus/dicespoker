#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Jeu de dés

# Depuis la librairie random on importe la fonction randint
# Randint permet de tirer un nombre aléatoire
from random import randint

# On initialise le tableau des scores des deux joueurs
# Les deux joueurs commencent à 0
scores = [0,0]

# Fonction de lancé d'un seul dé
# Le résultat sera un chiffre entre 1 et 6, inclus
def dice(vmin, vmax):
    # Le résultat est un nombre aléatoire entre min et max
    result = randint(vmin, vmax)
    return(result)

# Fonction d'évaluation du résultat d'un tirage
def check(dices):
    pairs = []                      # Liste des paires
    triple = []                     # Liste des triples
    four = []                       # Liste des quadruples
    quinte = []                     # Liste des quintuples
    straight1 = [1, 2, 3, 4, 5]     # La suite faible
    straight2 = [2, 3, 4, 5, 6]     # La suite forte
    score = 0                       # Le score de ce lancé est mis à 0

    # Pour chaque dés du tirage on va chercher à savoir combien
    # d'autre dés ont le même chiffre et donc ajouter les chiffres
    # en double à une liste des paires, en triples à la liste des triples, etc.
    # dices.count(d) veut dire : dans la liste "dices" combien de fois trouve t'on le chiffre "d"

    # Test du nombre de paires ou doubles
    for d in dices:
        if dices.count(d) > 1 and dices.count(d) < 3:
            pairs.append(d)

    # Test du nombre de triples
        if dices.count(d) == 3:
            triple.append(d)
    
    # Test du nombre de quadrules
        if dices.count(d) == 4:
            four.append(d)
    
    # Test du nombre de quintuples
        if dices.count(d) == 5:
            quinte.append(d)

    # On trie les dès pour comparer plus tard avec les suites
    dices.sort()

    # Cette condition est source de bug si on ne test pas ce qu'il y a dans triple !
    # Si la liste pairs n'a qu'une paire
    if len(pairs) == 2 and len(triple) != 3 :
        print(" Tu as une paire de {}".format(pairs[0]))
        score = 1
    
    # Si la liste pairs a deux paires
    elif len(pairs) == 4:
        pairs.sort()
        print(" Tu as une paires de {} et une paire de {}".format(pairs[0], pairs[2]))
        score = 2
    
    # Si la liste triple contient 3 chiffres et que la liste pairs est vide
    elif len(triple) == 3 and len(pairs) != 2:
        print("Tu as un Brelan de {}".format(triple[0]))
        score = 3

    # Si la liste triple contient 3 chiffre et que la liste pairs en contient 2
    elif len(triple) == 3 and len(pairs) == 2:
        print(" Tu as un Full ! Trois {} et une paire de {}".format(triple[0], pairs[0]))
        score = 5
    
    # Si la liste quadruple contient 4 chiffres
    elif len(four) == 4:
        print(" Tu as un Carré de {}".format(four[0]))
        score = 8
    
    # Si le tirage contient la première ou la seconde suite
    # cette condition ne fonctionne pas si on NE trie pas dices !
    elif dices == straight1 or dices == straight2:
        print(" Tu as une suite ! {}".format(dices))
        score = 10
    
    # Si la liste quintuble contient bien 5 chiffres
    elif len(quinte) == 5:
        print(" Tu as une Quinte ! Cinq {} !".format(quinte[0]))
        score = 15

    else:
        print(" Désolé, tu n'a pas de combinaison !")

    return score

def reroll(p, tempDices):
    for i in range(5):
        
        # On veut donner la possibilité de relancer chaque dé du tirage.
        # On fait une boucle qui fait donc 5 tours
        # (range(0,5) correspond à : 0, 1, 2, 3, 4 puisque le second chiffre de range, 5 est la limite de stop
        # donc le tour 0 correspond au dés N°, le tour 1 au dés N°2, le tour 3 au des N° 3 etc
        # À chaque tour on affiche le tirage actualisé, en mettant le dé concerné entre [ ]
        if i == 0 :
            print(" [{}], {}, {}, {}, {}".format(tempDices[0], tempDices[1], tempDices[2], tempDices[3], tempDices[4]))
        elif i == 1 :
            print(" {}, [{}], {}, {}, {}".format(tempDices[0], tempDices[1], tempDices[2], tempDices[3], tempDices[4]))
        elif i == 2 :
            print(" {}, {}, [{}], {}, {}".format(tempDices[0], tempDices[1], tempDices[2], tempDices[3], tempDices[4]))
        elif i == 3 :
            print(" {}, {}, {}, [{}], {}".format(tempDices[0], tempDices[1], tempDices[2], tempDices[3], tempDices[4]))
        elif i == 4 :
            print(" {}, {}, {}, {}, [{}]".format(tempDices[0], tempDices[1], tempDices[2], tempDices[3], tempDices[4]))
        
        r = input(" Joueur {} : Conserver ce {} ou Relancer le dé ? (c/r) :".format(p, tempDices[i]))
        if r == "r" or r == "R":
            tempDices[i] = dice(1,6)
            
    # On trie le tirage, en ordre croissant des valeurs obtenues
    tempDices.sort()
    print(" Le tirage final du joueur {} est : {}, {}, {}, {}, {}".format(p, tempDices[0], tempDices[1], tempDices[2], tempDices[3], tempDices[4]))
    return tempDices

def roll(p):
    # score temporaire : sert car on doit pouvoir relancer les dès donc modifier le score
    tempScore = 0

    # Score définitif
    score = 0

    # Liste des tirages de dés du joueur
    dicesPlayer = []

    # On lance un tirage initial de 5 dés
    # Une boucle de 5 tours (0, 1, 2, 3, 4)
    for d in range(5) :
        # À chaque tour on appelle la fonction dice qui a besoin de deux valeurs, minimum et maximum
        # On ajoute chaque tirage à une liste dicesPlayer
        result = dice(1,6)
        dicesPlayer.append(result)
    
    # On trie le contenu de dicesPlayer, dans l'ordre croissant
    dicesPlayer.sort()
    # On affiche le contenu di tirage
    print(" Premier tirage pour le joueur {} : {}, {}, {}, {}, {}".format(p, dicesPlayer[0], dicesPlayer[1], dicesPlayer[2], dicesPlayer[3], dicesPlayer[4]))
    # On donne le tirage à la fonction qui vérifie si on a des combinaison et qui vérifie les tirages.
    tempScore = check(dicesPlayer)
    
    # On demande si le jouer accepte le tirage ou si on doit relancer des dés
    r = input(" Joueur {}: Conserver ce tirage ou Relancer des dès ? (c/r) :".format(p))
    
    # Si le joueur veux relancer des dés, on a besoin d'un second tirage, la fonction reroll
    if r == "r" or r == "R":
        # Pour obtenir le second tirage, on donne à la fonction reroll le N° du joueur en cours et son premier tirage
        # dicesPlaye récupère le résultat de la fonction reroll
        dicesPlayer = reroll(p, dicesPlayer)

        # On donne le contenu de dicesPlayer à la fonction qui détermine si il y a des combinaison et le score
        score = check(dicesPlayer)
        # On renvoit le score final à la boucle principale
        return score
    
    # Si non, si le joueur est satisfait de son premier tirage, le résultat de la fonction roll est score
    else :
        # Le score temporaire devient le score final
        score = tempScore
        return score

# On créé la boucle infinie du jeu
while(True):

    # On créé une boucle de 2 tours, 1 tour par joueur
    # On utilise range(2) pour créer les tour
    # Attention range() compte à partir de 0 et jusqu'à 1 puisque 2 est la limite interdite
    for tour in range(2):
        # Quand tour == 0 c'est celui du joueur 1
        if tour == 0:
            player = 1
        # Si non, cela signifie que c'est le tour du jouer 2
        else:
            player = 2
        
        # On va utiliser une nouvelle formulation pour print
        # print(" C'est le tour du jouer {}".format(player))
        # est la même chose que :
        # print(" C'est le tour du joueur " + str(player))

        print(" C'est le tour du joueur {}".format(player))
        
        # scores[] est une liste, elle contient [score joueur 1, score joueur 2]
        # Rappel, dans les liste la première valeur est à l'emplacement 0, la seconde à l'emplacement 1
        # Donc dans score[0] il y a le score du joueur 1
        # Et dans score[1] il y a le score du joueur 2
        # Donc pour peut se servir de la valeur de tour, pour ranger les scores
        
        # Il y a peut être déjà un score dans la case score[tour], résultat d'un tour précédent,
        # alors on y ajoute le résultat qui va être celui du tour en cours.
        # C'est la fonction roll(player ) qui va générer et renvoyer le scode du joueur dont c'est le tour.
        # On ajoute donc le résultat de roll(player), au contenu de la case score[tour]
        scores[tour] = scores[tour] + roll(player)
        
        # Puis on afficher ce score.
        # Le print suivant est l'équivalent de :
        # print(" Score du joueur " + str(player) + " : " + str(score[tour]) + " points")
        print(" Score du joueur {} : {} points".format(player, scores[tour]))
        print("")

    # Fin de partie ou continuer
    print(" Les scores sont de : ")
    print(" -> Joueur 1 : {} points".format(scores[0]))
    print(" -> Joueur 2 : {} points".format(scores[1]))
    
    r = input(" Voulez vous continuer la partie ? (o/n) :")
    if r != "o":
        if scores[0] > scores[1]:
            print(" *** Le jouer 1 gagne la partie ! ***")
        elif scores[1] > scores[0]:
            print(" *** Le jouer 2 gagne la partie ! ***")
        elif scores[0] == scores[1]:
            print(" *** Égalité ! ***")
        
        print(" À bientôt !")
        break
